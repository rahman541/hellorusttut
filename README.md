# Intro
Rust Programming Language tutorial Hello World.

# Build & Run
```
cargo build
cargo run
```

https://www.rust-lang.org/learn/get-started