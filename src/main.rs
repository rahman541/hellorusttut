use ferris_says::say;
use std::io::{ stdout, BufWriter };

fn main() {
    let mut writer = BufWriter::new(stdout());
    say(b"Hello fellow Rahman Romli!", 24, &mut writer).unwrap();
}
